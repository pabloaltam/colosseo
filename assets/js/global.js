$('select').select2({
    theme: "bootstrap",
    placeholder: "Seleccione",
    width: "100%",
    allowClear: true
});
$('#areas').select2({
    theme: "bootstrap",
    placeholder: "Seleccione",
    width: "100%",
    allowClear: false
});
$('[data-toggle="tooltip"]').tooltip();