<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuarios_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
        
    function obtener_usuarios(){
        $this->db->select("*")->from("MV_USUARIOS");
        $query = $this->db->get();
        return ($query->num_rows()>0) ? $query->result() : FALSE;
    }

}