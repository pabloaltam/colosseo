<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container" >
	<div class="row" style="margin-top: 50px;">
        <div class="col-md-8" style="margin-top: 50px;">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors() ?>
                </div>
            <?php endif; ?>
            <?php if (isset($error)) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
		<div class="col-md-offset-4 col-md-4">
			<div class="page-header">
				<h1>Iniciar sesión</h1>
			</div>
			<?= form_open() ?>
            <div class="panel panel-default">

                <div class="panel-body">
                  <img src="../assets/img/btn_google_signin_dark_normal_web.png" alt="" style="margin-bot:20px;">

                    <div class="form-group">
                        <label for="" class="control-label">Correo electrónico</label>
                        <input type="text" maxlength="40" class="form-control input-lg" name="username" placeholder="hola@hola.cl">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Constraseña</label>
                        <input type="password" maxlength="7" class="form-control input-lg" name="password" placeholder="*******">
                    </div>
                    <a href="#">Olvidé mi contraseña</a>
                </div>
                <div class="panel-footer clearfix">
                    <button type="submit" class="btn btn-default pull-right">Entrar</button>
                    <a class="btn btn-danger" href="<?php echo base_url('user/register');?>">Registrar</a>
                </div>
            </div>
            <?= form_close(); ?>
		</div>
	</div><!-- .row -->
</div><!-- .container -->
