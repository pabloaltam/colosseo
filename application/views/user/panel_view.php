<?php
/**
 * Created by PhpStorm.
 * User: Pablo
 * Date: 25-09-2017
 * Time: 20:46
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="row" style="margin-top: 100px;">
        <div class="col-sm-2">
            <?php var_dump($user);?>
            <?php var_dump($usuario);?>
            <!--<a href="<?php echo base_url('panel/adverts') ?>" class="btn btn-default btn-block btn-compose-email">Avisos</a>-->
            <ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-user-circle-o" style="font-size: inherit;"></i> Mi perfil
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-search"></i> Buscar avisos</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bookmark-o"></i> Avisos guardados</a>
                </li>
            </ul><!-- /.nav

            <h5 class="nav-email-subtitle">More</h5>
            <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow">
                <li>
                    <a href="#">
                        <i class="fa fa-folder-open"></i> Promotions  <span class="label label-danger pull-right">3</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-folder-open"></i> Job list
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-folder-open"></i> Backup
                    </a>
                </li>
            </ul><!-- /.nav -->
        </div>
        <div class="col-sm-8">
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-circle"></i>
                Debes completar tu perfil.
            </div>
            <!-- resumt -->
            <!--<div class="panel panel-default">
                <div class="panel-heading resume-heading">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-xs-12 col-sm-4">
                                <figure>
                                    <img class="img-circle img-responsive" alt="" src="http://placehold.it/200x200">
                                </figure>
                                <div class="row">
                                    <div class="col-xs-12 social-btns">
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-google">
                                                <i class="fa fa-google"></i> </a>
                                        </div>
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-facebook">
                                                <i class="fa fa-facebook"></i> </a>
                                        </div>
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-twitter">
                                                <i class="fa fa-twitter"></i> </a>
                                        </div>
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-linkedin">
                                                <i class="fa fa-linkedin"></i> </a>
                                        </div>
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-github">
                                                <i class="fa fa-github"></i> </a>
                                        </div>
                                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                            <a href="#" class="btn btn-social btn-block btn-stackoverflow">
                                                <i class="fa fa-stack-overflow"></i> </a>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <ul class="list-group">
                                    <li class="list-group-item">John Doe</li>
                                    <li class="list-group-item">Software Engineer</li>
                                    <li class="list-group-item">Google Inc. </li>
                                    <li class="list-group-item"><i class="fa fa-phone"></i> 000-000-0000 </li>
                                    <li class="list-group-item"><i class="fa fa-envelope"></i> john@example.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Summary</h4>
                    <p>
                        Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur. Quis verear mel ne. Munere vituperata vis cu,
                        te pri duis timeam scaevola, nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                    </p>
                    <p>
                        Odio recteque expetenda eum ea, cu atqui maiestatis cum. Te eum nibh laoreet, case nostrud nusquam an vis.
                        Clita debitis apeirian et sit, integre iudicabit elaboraret duo ex. Nihil causae adipisci id eos.
                    </p>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Research Interests</h4>
                    <p>
                        Software Engineering, Machine Learning, Image Processing,
                        Computer Vision, Artificial Neural Networks, Data Science,
                        Evolutionary Algorithms.
                    </p>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Prior Experiences</h4>
                    <ul class="list-group">
                        <a class="list-group-item inactive-link" href="#">
                            <h4 class="list-group-item-heading">
                                Software Engineer at Twitter
                            </h4>
                            <p class="list-group-item-text">
                                Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur. Quis verear mel ne. Munere vituperata vis cu,
                                te pri duis timeam scaevola, nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                            </p>
                        </a>
                        <a class="list-group-item inactive-link" href="#">
                            <h4 class="list-group-item-heading">Software Engineer at LinkedIn</h4>
                            <p class="list-group-item-text">
                                Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
                                Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
                                nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                            </p>
                            <ul>
                                <li>
                                    Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
                                    Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
                                    nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                                </li>
                                <li>
                                    Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
                                    Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
                                    nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                                </li>
                            </ul>
                            <p></p>
                        </a>
                    </ul>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Key Expertise</h4>
                    <ul class="list-group">
                        <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc </li>
                        <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
                        <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
                        <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
                        <li class="list-group-item">Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
                        <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
                    </ul>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Language and Platform Skills</h4>
                    <ul class="list-group">
                        <a class="list-group-item inactive-link" href="#">
                            <div class="progress">
                                <div data-placement="top" style="width: 80%;"
                                     aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-success">
                                    <span class="sr-only">80%</span>
                                    <span class="progress-type">Java/ JavaEE/ Spring Framework </span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                                    <span class="sr-only">70%</span>
                                    <span class="progress-type">PHP/ Lamp Stack</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                                    <span class="sr-only">70%</span>
                                    <span class="progress-type">JavaScript/ NodeJS/ MEAN stack </span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 65%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-warning">
                                    <span class="sr-only">65%</span>
                                    <span class="progress-type">Python/ Numpy/ Scipy</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning">
                                    <span class="sr-only">60%</span>
                                    <span class="progress-type">C</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 50%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning">
                                    <span class="sr-only">50%</span>
                                    <span class="progress-type">C++</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div data-placement="top" style="width: 10%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-danger">
                                    <span class="sr-only">10%</span>
                                    <span class="progress-type">Go</span>
                                </div>
                            </div>
                            <div class="progress-meter">
                                <div style="width: 25%;" class="meter meter-left"><span class="meter-text">I suck</span></div>
                                <div style="width: 25%;" class="meter meter-left"><span class="meter-text">I know little</span></div>
                                <div style="width: 30%;" class="meter meter-right"><span class="meter-text">I'm a guru</span></div>
                                <div style="width: 20%;" class="meter meter-right"><span class="meter-text">I''m good</span></div>
                            </div>
                        </a>
                    </ul>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Education</h4>
                    <table class="table table-striped table-responsive ">
                        <thead>
                        <tr>
                            <th>Degree</th>
                            <th>Graduation Year</th>
                            <th>CGPA</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Masters in Computer Science and Engineering</td>
                            <td>2014</td>
                            <td> 3.50 </td>
                        </tr>
                        <tr>
                            <td>BSc. in Computer Science and Engineering</td>
                            <td>2011</td>
                            <td> 3.25 </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>-->

            <div class="panel panel-default">
                <div class="panel-body">


                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#datos_personales" aria-controls="home" role="tab" data-toggle="tab">Datos personales</a></li>
                            <li role="presentation"><a href="#competencias" aria-controls="messages" role="tab" data-toggle="tab">Competencias</a></li>
                            <li role="presentation"><a href="#experiencia" aria-controls="messages" role="tab" data-toggle="tab">Experiencia</a></li>
                        </ul>
                        <form>
                        <!-- Tab panes -->
                        <div class="tab-content" style="margin-top: 10px;">
                            <div role="tabpanel" class="tab-pane active" id="datos_personales">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Rut</label>
                                                <input type="email" class="form-control " id="email" placeholder="12345678-9">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Nombre</label>
                                                <input type="email" class="form-control " id="email" value="<?php echo isset($user) ? $user->name : "";?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Segundo nombre (<abbr title="Opcional">opc.</abbr>)</label>
                                                <input type="email" class="form-control " id="email">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="form-group">
                                                <label for="pwd">Apellido paterno</label>
                                                <input type="text" class="form-control " id="pwd" value="<?php echo isset($user) ? $user->surname : "";?>">
                                            </div>

                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="pwd">Apellido materno</label>
                                                <input type="password" class="form-control " id="pwd">
                                            </div>

                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="presenta">Presentación</label>
                                            <textarea id="presenta" class="form-control" style="resize: none;   "></textarea>
                                        </div>
                                    </div>
                                </div>






                            </div>
                            <div role="tabpanel" class="tab-pane" id="competencias">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="idioma">Manejo de idiomas</label>
                                            <select id="idioma">
                                                <option></option>
                                                <option>Francés</option>
                                                <option>Inglés</option>
                                                <option>Portugués</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="idioma">Nivel</label>
                                            <select id="idioma">
                                                <option></option>
                                                <option>Básico</option>
                                                <option>Medio</option>
                                                <option>Avanzado</option>
                                                <option>Profesional</option>
                                                <option>Nativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="idioma">Nivel de computación</label>
                                            <select id="idioma">
                                                <option></option>
                                                <option>Básico</option>
                                                <option>Medio</option>
                                                <option>Avanzado</option>
                                                <option>Profesional</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="experiencia">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="exp">Trabajos anteriores</label>
                                            <textarea class="form-control" id="exp"></textarea>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="areas">Áreas en las que se ha desempeñado</label>
                                            <select id="areas" multiple>
                                                <option></option>
                                                <option>Obras Civiles</option>
                                                <option>Obras Fiscales</option>
                                                <option>Pavimentación</option>
                                                <option>Vialidad</option>
                                                <option>MOP</option>
                                                <option>SERVIU</option>
                                                <option>MUNICIPALIDADES</option>
                                            </select>
                                            <p class="help-block">Puede seleccionar más de una.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="rubros">Rubros</label>
                                            <select id="rubros" multiple>
                                                <option></option>
                                                <option>Obras Civiles</option>
                                                <option>Obras Fiscales</option>
                                                <option>Pavimentación</option>
                                                <option>Vialidad</option>
                                                <option>MOP</option>
                                                <option>SERVIU</option>
                                                <option>MUNICIPALIDADES</option>
                                            </select>
                                            <p class="help-block">Puede seleccionar más de una.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="obras">Obras</label>
                                            <select id="obras" multiple>
                                                <option></option>
                                                <option>Portuarias</option>
                                                <option>Hidráulicas</option>
                                                <option>Civiles</option>
                                                <option>Viviendas en serie</option>
                                            </select>
                                            <p class="help-block">Puede seleccionar más de una.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

        </div>
                <div class="panel-footer clearfix">
                    <button type="submit" class="btn btn-primary btn-sm pull-right">Guardar</button>
                </div>
            </div>
</div>
