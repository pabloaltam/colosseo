<?php
/**
 * Created by PhpStorm.
 * User: Pablo
 * Date: 01-10-2017
 * Time: 23:41
 */
?>
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h3>Avisos</h3>
            <hr>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="#">
                                <img class="media-object" src="http://via.placeholder.com/64x64" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Tesla motors</h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad cupiditate deleniti dicta ea explicabo fugiat incidunt iure laudantium magni minima nostrum nulla, provident, quisquam reprehenderit tempore veritatis vero. Autem, rerum!
                            <hr>
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs "><i class="fa fa-plus"></i> Más información</button>
                                <button class="btn btn-danger btn-xs"><i class="fa fa-heart"></i></button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="#">
                                <img class="media-object" src="http://via.placeholder.com/64x64" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">SpaceX</h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad cupiditate deleniti dicta ea explicabo fugiat incidunt iure laudantium magni minima nostrum nulla, provident, quisquam reprehenderit tempore veritatis vero. Autem, rerum!
                            <hr>
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs "><i class="fa fa-plus"></i> Más información</button>
                                <button class="btn btn-danger btn-xs"><i class="fa fa-heart"></i></button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="#">
                                <img class="media-object" src="http://via.placeholder.com/64x64" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Google Inc.</h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur earum hic nesciunt nobis non veritatis! Architecto debitis, deserunt et iusto laudantium maxime quam saepe sint sit voluptatem? Excepturi, minima.
                            <hr>
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs "><i class="fa fa-plus"></i> Más información</button>
                                <button class="btn btn-danger btn-xs"><i class="fa fa-heart"></i></button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    TEST
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    TEST
                </div>
            </div>
        </div>

    </div>
</div>
