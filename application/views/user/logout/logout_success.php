<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Hasta pronto!</h1>
			</div>
			<p>Te estaremos esperando.</p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->