<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-md-offset-4 col-md-4">
			<div class="page-header">
				<h1>Ingresa tus datos</h1>
			</div>
			<?= form_open('', array('class' => '')) ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class=" col-sm-6">
                            <div class="form-group">
                                <label for="username">Nombre</label>
                                <input type="text" class="form-control " id="username" name="name" placeholder="Juanito">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="username">Apellido</label>
                                <input type="text" class="form-control " id="username" name="surname" placeholder="Pérez">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="username">Correo electrónico</label>
                                <input type="text" class="form-control " id="username" name="username" placeholder="hola@hola.com">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control " id="password" name="password" placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_confirm">Repetir contraseña</label>
                                <input type="password" class="form-control " id="password_confirm" name="password_confirm" placeholder="Contraseña">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <input type="submit" class="btn btn-primary btn-lg pull-right" value="Registrarme">
                </div>
            </div>


			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->