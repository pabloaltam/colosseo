<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Tu registro ha sido exitoso!</h1>
			</div>
			<p>A continuación, por favor completa tu perfil.</p>
            <a href="<?= base_url('panel');?>" class="btn btn-danger btn-lg"><i class="fa fa-user"></i> Perfil</a>
		</div>
	</div><!-- .row -->
</div><!-- .container -->