<?php
/**
 * Created by PhpStorm.
 * User: Pablo
 * Date: 23-09-2017
 * Time: 2:21
 */

?>
<div class="section section-header">

    <div class="parallax filter">
        <div class="image" style="background-image: url('<?php echo base_url('assets/img/principal/colosseo-principal.png'); ?>')">
        </div>
        <div class="container">
            <div class="content">
                <div class="title-area">
                    <img width="60%" height="60%" src="<?php echo base_url('assets/img/logo/logo07_blanco.png'); ?>"/>
                    <div class="separator line-separator">♦</div>
                </div>

                <div class="button-get-started">
                    <a href="<?php echo base_url('user/login'); ?>" class="btn btn-white btn-fill btn-lg ">
                        Ingresar a las Mazamorras
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="section">
    <div class="container">
        <div class="row">
            <div class="title-area">
                <h2>Nuestros Servicios</h2>
                <div class="separator separator-colosseo">✻</div>
                <p class="description">Somos un sistema de emparejamiento deportivo que busca encontrar un rival a tu equipo, haciendo coincidir el lugar, la hora y su nivel de habilidades.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info-icon">
                    <div class="icon text-success">
                        <i class="pe-7s-graph1"></i>
                    </div>
                    <h3>Sistema de Puntación ELO</h3>
                    <p class="description">Permite registrar el avance de tus partidos y emparejarte con gente de tu nivel correspondiente.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-icon">
                    <div class="icon text-success">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <h3>Gestión de Recientos Deportivos</h3>
                    <p class="description">Gestiona tus instalaciones con nosotros, permitiendo que cientos de usuarios puedan hacer uso de ellas .</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-icon">
                    <div class="icon text-success">
                        <i class="pe-7s-users"></i>
                    </div>
                    <h3>Invita a tus Amigos</h3>
                    <p class="description">Compite con tus amigos, forma tu equipo y nosotros llamamos a los que falten además del rival.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="section section-our-team-freebie">
    <div class="parallax filter filter-color-black">
        <div class="image" style="background-image:url('<?php echo base_url('assets/img/principal/colosseo-promo.jpeg'); ?>')">
        </div>
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="title-area">
                        <h2>Ranking Individual/Equipos</h2>
                        <div class="separator separator-colosseo">✻</div>
                        <p class="description">Demuestra quién es el mejor jugador de la región.</p>
                    </div>
                </div>

                <div class="team">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-member">
                                        <div class="content">
                                            <div class="avatar avatar-colosseo">
                                                <img alt="..." class="img-circle" src="<?php echo base_url('assets/img/principal/soccerplayer.jpg'); ?>"/>
                                            </div>
                                            <div class="description">
                                                <h3 class="title">Soy Leyenda</h3>
                                                <p class="small-text">Ranking Individual</p>
                                                <p class="description">Busca partido solo y demuestra tu potencial al mundo, serás emparejado con otros jugadores de tu mismo nivel y podrás jugar cuando quieras.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-member">
                                        <div class="content">
                                            <div class="avatar avatar-colosseo">
                                                <img class="img-circle" src="<?php echo base_url('assets/img/principal/Andrea.jpg'); ?>"/>
                                            </div>
                                            <div class="description">
                                                <h3 class="title">Un Caballero</h3>
                                                <p class="small-text">Ranking de los jugadores mejor evaluados</p>
                                                <p class="description">No todo es magia y goles, tambien hay que ser un caballero dentro y fuera de la cancha, es por ello que hemos diseñado un ranking con los jugadores mejor evaluados de la comunidad.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-member">
                                        <div class="content">
                                            <div class="avatar avatar-colosseo">
                                                <img alt="..." class="img-circle" src="<?php echo base_url('assets/img/principal/team.png'); ?>"/>
                                            </div>
                                            <div class="description">
                                                <h3 class="title">Los de Siempre</h3>
                                                <p class="small-text">Ranking por Equipos</p>
                                                <p class="description">Se el lider de la tabla ganando la mayor cantidad de partidos posibles, entre mas juegues más puntos podrán ganar.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="section section-our-clients-freebie">
    <div class="container">
        <div class="title-area">
            <h5 class="subtitle text-gray">La comunidad deportiva</h5>
            <h2>Testimonio de Usuarios</h2>
            <div class="separator separator-colosseo">∎</div>
        </div>

        <ul class="nav nav-text" role="tablist">
            <li class="active">
                <a href="#testimonial1" role="tab" data-toggle="tab">
                    <div class="image-clients">
                        <img class="img-circle" width="100" height="100" src="<?php echo base_url('assets/img/principal/sarqo.png'); ?>"/>
                    </div>
                </a>
            </li>
            <li>
                <a href="#testimonial2" role="tab" data-toggle="tab">
                    <div class="image-clients">
                        <img alt="..." class="img-circle" width="100" height="100" src="<?php echo base_url('assets/img/principal/jorge-retamal.jpg'); ?>"/>
                    </div>
                </a>
            </li>
            <li>
                <a href="#testimonial3" role="tab" data-toggle="tab">
                    <div class="image-clients">
                        <img alt="..." class="img-circle" width="100" height="100" src="<?php echo base_url('assets/img/principal/BIG-G.png'); ?>"/>
                    </div>
                </a>
            </li>
        </ul>


        <div class="tab-content">
            <div class="tab-pane active" id="testimonial1">
                <p class="description">
                    "La aplicación cumple con todo lo que uno espera, funciona a la perfección y hará que toda una generación se vuelva a enamorar del deporte."
                </p>
            </div>
            <div class="tab-pane" id="testimonial2">
                <p class="description">"Es el proyecto más revolucionario y ambicioso que a salido de la región, la simplicidad de la idea planteada a la perfección entrega una experiencia realmente hermosa al usuario."
                </p>
            </div>
            <div class="tab-pane" id="testimonial3">
                <p class="description">"No juego futbol pero ta wena hiii."
                </p>
            </div>

        </div>

    </div>
</div>


<div class="section section-small section-get-started">
    <div class="parallax filter filter-color-black">
        <div class="image" style="background-image:url('<?php echo base_url('assets/img/principal/cancha.jpg'); ?>')">
        </div>
        <div class="container">
            <div class="title-area">
                <h2 class="text-white">Se parte de la Red Colosseo</h2>
                <div class="separator line-separator">♦</div>
                <p class="description">Se parte de la red deportiva más revolucionaria del pais, con un completo sistema de gestión para tus instalaciones.</p>
            </div>

            <div class="button-get-started">
                <a href="#gaia" class="btn btn-white btn-fill btn-lg">Contactanos</a>
            </div>
        </div>
    </div>
</div>
