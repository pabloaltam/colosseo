<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('usuarios_model');
		$this->load->helper('form');
		$data['data_usuarios'] = $this->usuarios_model->obtener_usuarios();
		$this->load->view('global/header_view');
		$this->load->view('landing/welcome_view', $data);
		$this->load->view('global/footer_view');
	}

    public function page1()
    {
        $this->session->set_flashdata('test', 'HELLO WORLD');
        //$this->session->keep_flashdata('test');
        redirect('user/page2','refresh');
    }
    public function page2()
    {
        redirect('user/page3','refresh');
    }
    public function page3()
    {
        var_dump($this->session->flashdata());
    }

    public function libs()
    {
        echo 'Currently loaded libs: '.implode(', ', $this->loader->_ci_classes());
    }
}
