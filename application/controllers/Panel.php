<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() {

        parent::__construct();
        $this->load->model('user_model');

    }
	public function index()
	{
	    $data_session = $this->session->flashdata('data_session');
	    $data['data'] = $data_session;
        $data['user']    = $this->user_model->get_user((int)$this->session->user_id);
		$this->load->view('global/header_view');
		$this->load->view('user/panel_view', $data);
		$this->load->view('global/footer_view');
	}

	public function adverts()
    {
        $this->load->view('global/header_view');
		$this->load->view('user/avisos_view');
		$this->load->view('global/footer_view');
    }
}
