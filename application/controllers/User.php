<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class User extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->model('user_model');
        $this->session->keep_flashdata('test');
				

	}


	public function index() {
        $this->load->view('global/header_view');
        $this->load->view('user/register/register');
        $this->load->view('global/footer_view');

	}

	/**
	 * register function.
	 *
	 * @access public
	 * @return void
	 */
	public function register() {

		// create the data object
		$data = new stdClass();

		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('username', 'Correo electrónico', 'trim|required|valid_email|min_length[4]|is_unique[users.username]', array('is_unique' => 'Este correo electrónico ya existe en nuestra base de datos.'));
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('surname', 'Apellido', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('password_confirm', 'Confirmar contraseña', 'trim|required|min_length[6]|matches[password]',array('matches' => 'Las contraseñas ingresadas no coinciden.'));

		if ($this->form_validation->run() === false) {

			// validation not ok, send validation errors to the view
			$this->load->view('global/header_view');
			$this->load->view('user/register/register', $data);
			$this->load->view('global/footer_view');

		} else {

			// set variables from the form
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
			$password = $this->input->post('password');
			$name     = $this->input->post('name');
			$surname  = $this->input->post('surname');

			if ($this->user_model->create_user($username, $email, $password, $name, $surname)) {


				// user creation ok
				//$this->load->view('global/header_view');
				//$this->load->view('user/register/register_success', $data);
				//$this->load->view('global/footer_view');
                $data_login['username'] = $username;
                $data_login['password'] = $password;
                $this->session->set_flashdata('data_login', $data_login);
				redirect(base_url('user/login'));
				if($$data_login['password']!==FALSE){
					$hola = "hola";
				}
			} else {

				// user creation failed, this should never happen
				$data->error = 'Ocurrió un error en el registro, por favor intenta nuevamente.';

				// send error to the view
				$this->load->view('global/header_view');
				$this->load->view('user/register/register', $data);
				$this->load->view('global/footer_view');

			}

		}

	}

	/**
	 * login function.
	 *
	 * @access public
	 * @return void
	 */
	public function login() {

		// create the data object
		$data = new stdClass();

		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules

        $data_login_registro = $this->session->flashdata('data_login');


        $this->form_validation->set_rules('username', 'Username', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === false && !isset($data_login_registro)) {

			// validation not ok, send validation errors to the view
			$this->load->view('global/header_view');
			$this->load->view('user/login/login');
			$this->load->view('global/footer_view');

		} else {
            if(isset($data_login_registro)){
                $_POST['username'] = $data_login_registro['username'];
                $_POST['password'] = $data_login_registro['password'];
            }
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // set variables from the form
			if ($this->user_model->resolve_user_login($username, $password)) {

				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);

				// set session user datas
				//$_SESSION['user_id']      = (int)$user->id;
				//$_SESSION['username']     = (string)$user->username;
				//$_SESSION['logged_in']    = (bool)true;
				//$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				//$_SESSION['is_admin']     = (bool)$user->is_admin;

                $arraydata = array(
                    'user_id'  => (int)$user->id,
                    'username'     => (string)$user->username,
                    'logged_in' => (bool)true,
                    'is_confirmed' => (bool)$user->is_confirmed,
                    'is_admin' => (bool)$user->is_admin,
                    'user' => $user,
                    'name' => (string)$user->name,
                    'surname' => (string)$user->surname
                );
                $this->session->set_userdata($arraydata);

				// user login ok
                $data_session['user'] = $user;
                $data_session['user_id'] = $user_id;

				//$this->load->view('global/header_view');
				//$this->load->view('user/login/login_success', $data_session);
				//$this->load->view('global/footer_view');

                //$this->session->set_flashdata('data_session',$data_session['user']);
                redirect(base_url('user/panel'));
			} else {

				// login failed
				$data->error = 'El correo electrónico o la contraseña ingresada son incorrectos.';

				// send error to the view
				$this->load->view('global/header_view');
				$this->load->view('user/login/login', $data);
				$this->load->view('global/footer_view');

			}

		}

	}

	/**
	 * logout function.
	 *
	 * @access public
	 * @return void
	 */
	public function logout() {

		// create the data object
		$data = new stdClass();

		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}

			// user logout ok
			$this->load->view('global/header_view');
			$this->load->view('user/logout/logout_success', $data);
			$this->load->view('global/footer_view');

		} else {

			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');

		}

	}

    public function panel()
    {
        $data['data'] = 'hola';
        $data['usuario'] = (int)$this->session->user_id;
        $data['user']    = $this->user_model->get_user((int)$this->session->user_id);
        $this->load->view('global/header_view');
        $this->load->view('user/panel_view', $data);
        $this->load->view('global/footer_view');
    }

    public function page1()
    {
        $this->session->set_flashdata('test', 'HELLO WORLD');
        redirect('user/page2','refresh');
    }
    public function page2()
    {
        var_dump($this->session->flashdata('test'));
    }
    public function page3()
    {

    }

}
