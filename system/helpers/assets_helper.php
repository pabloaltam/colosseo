<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function cargarJS($jsCache = false) {
    $path_routes = obtener_path_routes("js");
    $path_absoluta = ASSETSPATH . $path_routes; // Descomposicion URI
    $js_path = "../assets/" . $path_routes; // ruta directorio para obtener js

    $cache = ($jsCache == false) ? '': '?v='.date('Ymd');
    foreach(obtener_archivos_por_ruta($path_absoluta, "js") as $js) {
        echo "<script type='text/javascript' src='" . $js_path . $js . $cache . "'></script>
";
    }
}

if (!function_exists('cargarCSS')) {
    function cargarCSS() {
        $path_routes = obtener_path_routes("css"); // Descomposicion URI
        $path_absoluta = ASSETSPATH . $path_routes; // ruta directorio para obtener css
        $css_path = base_url("/assets/" . $path_routes); // ruta para cargar css

        foreach(obtener_archivos_por_ruta($path_absoluta, "css") as $css) {
            echo "<link href='" . $css_path . $css . "' rel='stylesheet'/>";
        }

        //echo $path_routes."<br>".$path_absoluta."<br>".$css_path;
        //var_dump(file_exists($path_absoluta));


    }
}

if (!function_exists('obtener_path_routes')) {
    function obtener_path_routes($ext) {
        $CI =& get_instance();
        $modulo = 'application';
        $controlador = $CI->router->fetch_class();
        $metodo = $CI->router->fetch_method();

        if ($CI->router->fetch_module()) {
            $modulo = "sistemas/" .  $CI->router->fetch_module();
        }

        return (($modulo) ? $modulo . "/" : "") . $ext . "/" . $controlador . "/" . $metodo . "/";
    }
}

if (!function_exists('obtener_archivos_por_ruta')) {
    function obtener_archivos_por_ruta($path, $ext) {
        $resultados = array();

        if(file_exists($path)) {
            foreach(glob($path . "*." . $ext) as $nombre_archivo) {
                $resultados[] = basename($nombre_archivo);
            }
        }

        return $resultados;
    }
}